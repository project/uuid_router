<?php

namespace Drupal\Tests\uuid_router\Unit\PathProcessor;

use Drupal\Tests\UnitTestCase;
use Drupal\uuid_router\PathProcessor\EntityUuidPathProcessor;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * @coversDefaultClass \Drupal\uuid_router\PathProcessor\EntityUuidPathProcessor
 * @group uuid_router
 */
class EntityUuidPathProcessorTest extends UnitTestCase {
  /**
   * The tested path processor.
   *
   * @var \Drupal\path_alias\PathProcessor\AliasPathProcessor
   */
  protected $pathProcessor;

  /**
   * Entity mocked type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $storage;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityType;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entity;

  /**
   * The canonical url.
   *
   * @var \Drupal\Core\Url|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $url;

  /**
   * The edit url.
   *
   * @var \Drupal\Core\Url|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $editUrl;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->storage = $this->createMock(EntityStorageInterface::class);
    $this->entityType = $this->createMock(EntityTypeInterface::class);
    $this->entity = $this->createMock(EntityInterface::class);
    $this->url = $this->createMock(Url::class);
    $this->editUrl = $this->createMock(Url::class);
    $this->pathProcessor = new EntityUuidPathProcessor($this->entityTypeManager);
  }

  /**
   * Tests the processInbound method.
   *
   * @see \Drupal\path_alias\PathProcessor\AliasPathProcessor::processInbound
   */
  public function testProcessInbound(): void {
    $entityTypeId = 'node';

    $uuid = '8dd72c38-f2f9-11ed-a05b-0242ac120003';
    $unKnownUuid = '8dd72c38-f2f9-11ed-a05b-0242ac120004';

    $nodePath = '/node/1';
    $nodeEditPath = '/node/1/edit';

    $defaultRelationShip = 'canonical';
    $editRelationShip = 'edit-form';

    $this->entityTypeManager->method('hasDefinition')
      ->willReturnMap([
        [$entityTypeId, TRUE],
      ]);

    $this->entityTypeManager->method('getStorage')
      ->willReturnMap([
        [$entityTypeId, $this->storage],
      ]);

    $this->entityTypeManager->method('getDefinition')
      ->willReturnMap([
        [$entityTypeId, TRUE, $this->entityType],
      ]);

    $this->entityType->method('hasLinkTemplate')
      ->willReturnMap([
        [$defaultRelationShip, TRUE],
        [$editRelationShip, TRUE],
      ]);

    $this->entity->method('toUrl')
      ->willReturnMap([
        [$defaultRelationShip, [], $this->url],
        [$editRelationShip, [], $this->editUrl],
      ]);

    $this->storage->method('loadByProperties')
      ->willReturnMap([
        [['uuid' => $uuid], [$this->entity]],
        [['uuid' => $unKnownUuid], []],
      ]);

    $this->url->method('getInternalPath')
      ->willReturnMap([
        [$nodePath],
      ]);

    $this->editUrl->method('getInternalPath')
      ->willReturnMap([
        [$nodeEditPath],
      ]);

    // Node request.
    $uri = "/$entityTypeId/$uuid";
    $request = Request::create($uri);
    $this->assertEquals($nodePath, $this->pathProcessor->processInbound($uri, $request));

    // Node edit request.
    $uri = "/$entityTypeId/$uuid/edit";
    $request = Request::create($uri);
    $this->assertEquals($nodeEditPath, $this->pathProcessor->processInbound($uri, $request));

    // Request with unknown uuid.
    $uri = "/$entityTypeId/$unKnownUuid";
    $request = Request::create($uri);
    $this->assertEquals($uri, $this->pathProcessor->processInbound($uri, $request));
  }

}
