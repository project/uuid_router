<?php

namespace Drupal\uuid_router\PathProcessor;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to transpose entity UUID paths to serialize IDs.
 *
 * Incoming paths matching /{entity_type_id}/{uuid} and
 * /{entity_type_id}/{uuid}/edit are routed to their correct path.
 *
 * @see https://drupal.org/i/2353611
 */
class EntityUuidPathProcessor implements InboundPathProcessorInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PathProcessorEntityUuid object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $matches = [];

    if (preg_match('/^\/([a-z_]+)\/(' . Uuid::VALID_PATTERN . ')$/i', $path, $matches)) {
      $relationship = 'canonical';
    }
    elseif (preg_match('/^\/([a-z_]+)\/(' . Uuid::VALID_PATTERN . ')\/edit$/i', $path, $matches)) {
      $relationship = 'edit-form';
    }
    else {
      return $path;
    }

    array_shift($matches);
    [$entityTypeId, $uuid] = $matches;

    return $this->getInternalPath($entityTypeId, $uuid, $relationship) ?? $path;
  }

  /**
   * Get the internal path for a given entity type and UUID.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $uuid
   *   The UUID.
   * @param string $relationship
   *   The relationship.
   *
   * @return string|null
   *   The internal path or NULL if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getInternalPath(string $entity_type_id, string $uuid, string $relationship): string|null {
    if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
      return NULL;
    }

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entityType = $this->entityTypeManager->getDefinition($entity_type_id);

    if ($entityType->hasLinkTemplate($relationship) && $entities = $storage->loadByProperties(['uuid' => $uuid])) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = reset($entities);
      return '/' . ltrim($entity->toUrl($relationship)->getInternalPath(), '/');
    }

    return NULL;
  }

}
